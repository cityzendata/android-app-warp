package com.cityzendata.warpapp;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

import java.lang.reflect.Field;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by ahebert on 12/14/15.
 */
public class SensorValue
{

    public static SensorEvent CreateSensorEvent(float value){
        //SensorEvent sensorEvent = Mockito.spy(SensorEvent.class);
        SensorEvent sensorEvent = mock(SensorEvent.class);
        try {
            Field valuesField = SensorEvent.class.getField("values");
            valuesField.setAccessible(true);
            float[] sensorValue = {value};
            try {
                valuesField.set(sensorEvent, sensorValue);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        return sensorEvent;
    }
}