package com.cityzendata.warpapp;

import android.hardware.SensorEvent;
import android.test.ServiceTestCase;

import junit.framework.Assert;

import java.util.LinkedList;


import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.mockito.Mockito;


/**
 * Created by ahebert on 12/11/15.
 */
public class SensorServiceTest extends ServiceTestCase<SensorService> {
    private SensorService sensorService;
    protected SensorEvent event;

    public SensorServiceTest(Class<SensorService> serviceClass, SensorService sensorService) {
        super(serviceClass);
        this.sensorService = sensorService;
    }

    public SensorServiceTest() {
        super(SensorService.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        System.setProperty("dexmaker.dexcache", getSystemContext().getCacheDir().getPath());
        sensorService = new SensorService();
        event = SensorValue.CreateSensorEvent(2);
    }

    public void testEmptyBufferNull() throws Exception {
        sensorService.url = "localhost";
        sensorService.token = "token";
        sensorService.emptyBuffer();
        assertEquals(sensorService.stringBuffer, null);
    }

    void aVoid(){}



    public void testEmptyBuffer() throws Exception {
        sensorService.url = "localhost";
        sensorService.token = "token";
        sensorService.stringBuffer = new StringBuffer("Test");
        //Mockito.doThrow(Exception.class).when(stub(FileService.class));
        //stub(FileService.writeToFile(anyString(), (Context) anyObject()));
        //Mockito.doThrow(Exception.class).when(stub(writeToFile(anyString(), (Context) anyObject())));
        //FileService.writeToFile(;
        //String numberToString = value.toString();
        sensorService.emptyBuffer();
        //assertEquals(sensorService.stringBuffer.toString(), new StringBuffer().toString());
    }

    public void testHandleEvent()
    {
        SensorService spyCollect = Mockito.spy(SensorService.class);
        Mockito.when(spyCollect.getSensorName(event)).thenReturn("name");
        when(spyCollect.getSensorType(event)).thenReturn(0);

        String gts = event.timestamp + "// " + spyCollect.prefixGTS + spyCollect.getSensorName(event).replace(" ", ".") + "{" +
                "type=" + sensorService.getSensorType(event) + ",vendor=" + event.sensor.getVendor().replace(" ", "-") +
                ",version=" + event.sensor.getVersion() + ",source=android" + ",valueIndex=0} 2";
        spyCollect.stringBuffer = new StringBuffer();
        //spyCollect.onSensorChanged(event);
        Assert.assertEquals(gts,spyCollect.stringBuffer.toString());
    }




    public void testSample() throws Exception {
        // you can mock concrete classes, not only interfaces
        LinkedList mockedList = mock(LinkedList.class);

        // stubbing appears before the actual execution
        when(mockedList.get(0)).thenReturn("first");

        // the following prints "first"
        Assert.assertEquals("first", mockedList.get(0));

        // the following prints "null" because get(999) was not stubbed
        Assert.assertNull(mockedList.get(999));
    }

    /*
    public void testPostData() throws Exception {
        Socket socket = null;
        DataOutputStream dataOutputStream = null;
        DataInputStream dataInputStream = null;
        DataInputStream dataInputStream2 = null;
        String textout = "test";
        String textin = "Nothing";
        try {
            socket = new Socket("localhost", 8888);
            dataOutputStream = new DataOutputStream(socket.getOutputStream());
            dataInputStream = new DataInputStream(socket.getInputStream());
            dataOutputStream.writeUTF(textout);
            //sensorService.postData(new StringBuffer("test"), "192.168.1.101:8888", "token");
            dataInputStream2 = new DataInputStream(socket.getInputStream());
            textin += dataInputStream.toString();
            textin += dataInputStream2.readUTF();

            PrintWriter mBufferOut = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
            mBufferOut.write(textout);
            BufferedReader mBufferIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            textin = mBufferIn.readLine();
        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            assertEquals("test", textin);
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            if (dataOutputStream != null) {
                try {
                    dataOutputStream.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            if (dataInputStream != null) {
                try {
                    dataInputStream.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
     }
        */

}
