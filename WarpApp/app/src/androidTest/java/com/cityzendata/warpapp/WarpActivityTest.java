package com.cityzendata.warpapp;

import android.app.Instrumentation;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.support.v7.widget.Toolbar;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.LargeTest;
import android.test.suitebuilder.annotation.MediumTest;
import android.test.suitebuilder.annotation.SmallTest;
import android.util.Log;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import junit.framework.Assert;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by ahebert on 12/11/15.
 */

public class WarpActivityTest extends ActivityInstrumentationTestCase2<WarpActivity> {

    private WarpActivity mActivity; // MyActivity is the class name of the app under test
    private LinearLayout lLayout;
    private TextView mFirstTestText;
    private EditText prefixGTS;
    private Toolbar toolbar;
    private Button startButton;
    private Button stopButton;


    public WarpActivityTest(Class<WarpActivity> activityClass) {
        super(activityClass);
    }
    public WarpActivityTest() {
        super(WarpActivity.class);
    }

    protected void setUp() throws Exception {
        super.setUp();
        System.setProperty("dexmaker.dexcache", getInstrumentation().getTargetContext().getCacheDir().getPath());
        setActivityInitialTouchMode(true);
        //Instrumentation mInstrumentation = getInstrumentation();
        mActivity = getActivity();
        mFirstTestText =
                (TextView) mActivity
                        .findViewById(R.id.textView);

      /*
       * Get a reference to the main layout of the app under test
       */
        lLayout = (LinearLayout) mActivity.findViewById(R.id.mainLayout);
      /*
       * Get a reference to the edit text of the app under test
       */
        prefixGTS = (EditText) mActivity.findViewById(R.id.prefixGTS);
      /*
       * Get a reference to the toolbar of the app under test
       */
        toolbar = (Toolbar) mActivity.findViewById(R.id.toolbar);
      /*
       * Get a reference to the start Button of the app under test
       */
        startButton = (Button) mActivity.findViewById(R.id.startButton);
      /*
       * Get a reference to the stop Button of the app under test
       */
        stopButton = (Button) mActivity.findViewById(R.id.stopButton);
    }

    @SmallTest
    public void testPreconditions() {
        assertNotNull("mActivity is null", mActivity);
        assertNotNull("mFirstTestText is null", mFirstTestText);
        assertNotNull("lLayout is null", lLayout);
        assertNotNull("prefixGTS is null", prefixGTS);
        assertNotNull("prefixGTS is null", toolbar);
        assertNotNull("prefixGTS is null", startButton);
        assertNotNull("prefixGTS is null", stopButton);
    }

    @SmallTest
    public void testMyFirstTestTextView_labelText() {
        final String expected =
                "Hello World!";
        final String actual = mFirstTestText.getText().toString();
        assertEquals(expected, actual);
    }

    @SmallTest
    public void testInitTextPrefixGTS_labelText() {
        final String expected =
                mActivity.getString(R.string.prefixGTS);
        final String actual = prefixGTS.getText().toString();
        assertEquals(new String(), actual);
    }

    public void testSample() throws Exception {
        // you can mock concrete classes, not only interfaces
        LinkedList mockedList = mock(LinkedList.class);

        // stubbing appears before the actual execution
        when(mockedList.get(0)).thenReturn("first");

        // the following prints "first"
        Assert.assertEquals("first", mockedList.get(0));

        // the following prints "null" because get(999) was not stubbed
        Assert.assertNull(mockedList.get(999));
    }

    @LargeTest
    public void testInitLayout_labelText() {
        SensorManager mSensorManager = (SensorManager) mActivity.getSystemService(Context.SENSOR_SERVICE);
        List<String> listSensorsName = new ArrayList<String>();
        for (Sensor sensor:mSensorManager.getSensorList(Sensor.TYPE_ALL)
             ) {
            listSensorsName.add(sensor.getName());
        }
        List<String> checkBoxName = new ArrayList<String>();

        for (int i = 0; i < lLayout.getChildCount(); i++)
        {
            if (lLayout.getChildAt(i) instanceof CheckBox)
            {
                CheckBox cb = (CheckBox) lLayout.getChildAt(i);
                assertTrue(listSensorsName.contains(cb.getText()));
                checkBoxName.add(cb.getText().toString());
            }
        }
        for(String sensor:listSensorsName)
        {
            assertTrue(listSensorsName.contains(sensor));
        }
        assertEquals(new ArrayList<CharSequence>(), mActivity.getAllCheckedSensors());
    }

    @MediumTest
    public void testCheckBoxSensor() {
        setActivityInitialTouchMode(true);
        Instrumentation mInstrumentation = getInstrumentation();
        LinearLayout lLayout = (LinearLayout) mActivity.findViewById(R.id.mainLayout);
        CheckBox checkBox = null;
        for (int i = 0; i < lLayout.getChildCount(); i++)
        {
            if (lLayout.getChildAt(i) instanceof CheckBox)
            {
                checkBox = (CheckBox) lLayout.getChildAt(i);
                break;
            }
        }
        final CheckBox finalCheckbox = checkBox;
        if(checkBox != null) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    WarpActivity activity = getActivity();
                    finalCheckbox.performClick();
                    assertTrue(finalCheckbox.isChecked());
                    assertEquals(finalCheckbox.getText(), activity.getAllCheckedSensors().get(0));
                    Log.d("TAGFC1", finalCheckbox.isChecked() + "");
                    activity.finish();
                }
            });
        }
    }

    public void testEditTextPrefixGTS()
    {
        assertNotNull(prefixGTS);
        assertTrue(prefixGTS.requestFocus());
        assertTrue(prefixGTS.hasFocus());
        // Check if the EditText is properly set:
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                WarpActivity activity = getActivity();
                prefixGTS.requestFocus();
                prefixGTS.setText("android");
                assertEquals("android", prefixGTS.getText().toString());
                activity.finish();;
            }
        });
    }
}
