package com.cityzendata.warpapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Created by ahebert on 12/18/15.
 * Class used to create an alarm Listener used to push data on Warp
 * Delay execution time is set up on creation
 */
public class AlarmReceiver extends BroadcastReceiver
{

    private static final String EXTRA_URL = "com.cityzendata.warpapp.extra.URL";
    private static final String EXTRA_TOKEN = "com.cityzendata.warpapp.extra.TOKEN";
    private static final String EXTRA_STOP = "com.cityzendata.warpapp.extra.STOP";

    /**
     * Method flushing all data stored in application every time the alarm is triggered
     * @param context main application context
     * @param intent Intent containing application parameters as warp url and the application write token
     */
    public void onReceive(Context context, Intent intent)
    {
        String warpUrl = intent.getStringExtra(EXTRA_URL);
        String warpToken = intent.getStringExtra(EXTRA_TOKEN);
        Boolean stop = intent.getBooleanExtra(EXTRA_STOP, true);
        FlushService.flushAllFiles("fill", context, warpUrl, warpToken, stop);
        Log.d("ALARM", "STOP: " + stop);
        /**
         * This case is executed when it's the last push of the application (Stop button was pressed by the user
         */
        if(stop)
        {
            // When last alarm post exec, put collect active to false
            SharedPreferences sharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(context);
            SharedPreferences.Editor ed = sharedPrefs.edit();
            ed.putBoolean("isActive", false);
            ed.commit();
        }
    }
}
