package com.cityzendata.warpapp;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by ahebert on 1/8/16.
 * Activity to set sensors settings page
 * See SensorsFragment to get more details
 */
public class SetSensorsActivity extends Activity {
    @Override
    /**
     * Method executing on Creation of this Menu (When there is a click on Settings item inside Menu)
     */
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content,
                new SensorsFragment()).commit();
    }
}
