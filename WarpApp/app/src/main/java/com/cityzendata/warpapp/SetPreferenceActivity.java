package com.cityzendata.warpapp;


import android.app.Activity;
import android.os.Bundle;

/**
 * Created by ahebert on 12/18/15.
 * Activity used to show on screen the main setting page (ToolBar)
 * See SettingsFragment for more details
 * */
public class SetPreferenceActivity extends Activity {

    @Override
    /**
     * Method creating the Menu
     */
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction().replace(android.R.id.content,
                new SettingsFragment()).commit();
    }
}