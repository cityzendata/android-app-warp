package com.cityzendata.warpapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;

/**
 * Created by ahebert on 1/13/16.
 */
public class AlarmFlushData extends BroadcastReceiver {
    Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        FileService.writeToFile("alarm", context);
        this.context = context;
        Handler h = new Handler();
        h.postDelayed(r, 1000);
    }

    Runnable r = new Runnable() {
        @Override
        public void run(){
            SharedPreferences sharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(context);
            String url = sharedPrefs.getString("url", "NULL");
            String token = sharedPrefs.getString("token","NULL");
            FlushService.flushAllFiles("fill", context, url, token, false);
            FlushService.sendNotification(context, "End of manuel flush", "Flush ended");
        }
    };
}
