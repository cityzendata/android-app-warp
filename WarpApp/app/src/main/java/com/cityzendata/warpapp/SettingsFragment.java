package com.cityzendata.warpapp;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;

/**
 * Created by ahebert on 12/18/15.
 * Class necessary that extends preference Fragment by loading the preference file of this application
 */
//TODO Clean this class
public class SettingsFragment extends PreferenceFragment {

    /**
     * On create add all items registered in preferences.xml
     * Doesn't print on screen isActive nor checkedGTD
     * @param savedInstanceState
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
        PreferenceScreen myPreferenceScreen = getPreferenceScreen();
        Preference myPreference = myPreferenceScreen.findPreference("isActive");
        myPreferenceScreen.removePreference(myPreference);
        myPreference = myPreferenceScreen.findPreference("checkedGTS");
        myPreferenceScreen.removePreference(myPreference);
        //SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        //String syncConnPref = sharedPref.getString(KEY_PREF_SYNC_CONN, "");
    }
}
